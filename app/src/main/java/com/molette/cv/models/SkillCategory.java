package com.molette.cv.models;

import java.util.List;

/**
 * Created by Mathieu on 24/05/2016.
 */

public class SkillCategory {

    private int icon;
    private String name;
    private List<Skill> skills;

    public SkillCategory(int icon, String name, List<Skill> skills) {
        this.icon = icon;
        this.name = name;
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
