package com.molette.cv.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Mathieu on 29/05/2016.
 */

public class Experience implements Parcelable {

    private String company;
    private String place;
    private String startMonth;
    private String endMonth;
    private String startYear;
    private String endYear;
    private String role;
    private String project;
    private String description;
    private int logo;

    public Experience(String company, String place, String startMonth, String endMonth, String startYear, String endYear, String role, String project, String description, int logo) {
        this.company = company;
        this.place = place;
        this.startMonth = startMonth;
        this.endMonth = endMonth;
        this.startYear = startYear;
        this.endYear = endYear;
        this.role = role;
        this.project = project;
        this.description = description;
        this.logo = logo;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.company);
        dest.writeString(this.place);
        dest.writeString(this.startMonth);
        dest.writeString(this.endMonth);
        dest.writeString(this.startYear);
        dest.writeString(this.endYear);
        dest.writeString(this.role);
        dest.writeString(this.project);
        dest.writeString(this.description);
        dest.writeInt(this.logo);
    }

    protected Experience(Parcel in) {
        this.company = in.readString();
        this.place = in.readString();
        this.startMonth = in.readString();
        this.endMonth = in.readString();
        this.startYear = in.readString();
        this.endYear = in.readString();
        this.role = in.readString();
        this.project = in.readString();
        this.description = in.readString();
        this.logo = in.readInt();
    }

    public static final Creator<Experience> CREATOR = new Creator<Experience>() {
        @Override
        public Experience createFromParcel(Parcel source) {
            return new Experience(source);
        }

        @Override
        public Experience[] newArray(int size) {
            return new Experience[size];
        }
    };
}
