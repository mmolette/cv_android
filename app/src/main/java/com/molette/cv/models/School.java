package com.molette.cv.models;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.sql.Driver;

/**
 * Created by Mathieu on 27/05/2016.
 */

public class School {

    private String name;
    private int startDate;
    private int endDate;
    private String place;
    private String topic;
    private int picture;

    public School(String name, int startDate, int endDate, String place, String topic, int picture) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.place = place;
        this.topic = topic;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStartDate() {
        return startDate;
    }

    public void setStartDate(int startDate) {
        this.startDate = startDate;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }
}
