package com.molette.cv.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.molette.cv.CVApp;
import com.molette.cv.R;

import java.util.List;

/**
 * Created by Mathieu on 01/06/2016.
 */

public class Portfolio implements Parcelable {

    public enum Portfolio_Type {
        ANDROID(CVApp.getContext().getString(R.string.portfolio_action_android)),
        WEB(CVApp.getContext().getString(R.string.portfolio_action_web));

        private String name = "";

        Portfolio_Type(String name){

            this.name = name;
        }

        public String toString(){

            return name;
        }
    }

    private int logo;
    private Portfolio_Type type;
    private String project;
    private String date;
    private List<String> technologies;
    private String description;
    private String url;

    public Portfolio(int logo, Portfolio_Type type, String project, String date, List<String> technologies, String description, String url) {
        this.logo = logo;
        this.type = type;
        this.project = project;
        this.date = date;
        this.technologies = technologies;
        this.description = description;
        this.url = url;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public Portfolio_Type getType() {
        return type;
    }

    public void setType(Portfolio_Type type) {
        this.type = type;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(List<String> technologies) {
        this.technologies = technologies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.logo);
        dest.writeSerializable(this.type);
        dest.writeString(this.project);
        dest.writeString(this.date);
        dest.writeStringList(this.technologies);
        dest.writeString(this.description);
        dest.writeString(this.url);
    }

    protected Portfolio(Parcel in) {
        this.logo = in.readInt();
        this.type = (Portfolio_Type) in.readSerializable();
        this.project = in.readString();
        this.date = in.readString();
        this.technologies = in.createStringArrayList();
        this.description = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<Portfolio> CREATOR = new Parcelable.Creator<Portfolio>() {
        @Override
        public Portfolio createFromParcel(Parcel source) {
            return new Portfolio(source);
        }

        @Override
        public Portfolio[] newArray(int size) {
            return new Portfolio[size];
        }
    };
}
