package com.molette.cv.models;

/**
 * Created by Mathieu on 24/05/2016.
 */

public class Skill {

    private int icon;
    private String name;

    public Skill(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
