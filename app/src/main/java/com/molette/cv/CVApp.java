package com.molette.cv;

import android.app.Application;
import android.content.Context;

/**
 * Created by Mathieu on 22/05/2016.
 */

public class CVApp extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }

}