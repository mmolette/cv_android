package com.molette.cv.helpers;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.molette.cv.CVApp;

import java.util.Locale;

/**
 * Created by Mathieu on 23/05/2016.
 */

public class InternationalizationHelper {

    public static void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = CVApp.getContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
