package com.molette.cv.helpers;

import android.content.Intent;
import android.net.Uri;

import com.molette.cv.CVApp;
import com.molette.cv.R;

/**
 * Created by Mathieu on 22/05/2016.
 */

public class ContactHelper {

    private static final String EMAIL_ADDRESS = "mathieumolette@gmail.com";
    private static final String APP_URL = "https://goo.gl/F1Q1sP";

    public static void sendEmail(){
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + EMAIL_ADDRESS));
        intent = Intent.createChooser(intent, CVApp.getContext().getString(R.string.chooser_email));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        CVApp.getContext().startActivity(intent);
    }

    public static void share(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, CVApp.getContext().getString(R.string.share_content) + " " + APP_URL);
        intent = Intent.createChooser(intent, CVApp.getContext().getString(R.string.chooser_share));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        CVApp.getContext().startActivity(intent);
    }
}
