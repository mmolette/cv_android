package com.molette.cv.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.molette.cv.R;
import com.molette.cv.fragments.ExperienceListFragment;
import com.molette.cv.models.Experience;

import java.util.List;


public class ExperiencesAdapter extends RecyclerView.Adapter<ExperiencesAdapter.ViewHolder> {

    private ExperienceListFragment.OnExperienceListFragmentInteractionListener mListener;
    private final List<Experience> mValues;

    public ExperiencesAdapter(List<Experience> items, ExperienceListFragment.OnExperienceListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_experience, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.experience = mValues.get(position);
        holder.mRoleView.setText(holder.experience.getRole());
        holder.mCompanyView.setText(holder.experience.getCompany());
        String startDate = holder.experience.getStartMonth() + " " + holder.experience.getStartYear();
        String endDate = holder.experience.getEndMonth() + " " + holder.experience.getEndYear();
        holder.mDateView.setText(startDate + " - " + endDate);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    mListener.onExperienceListFragmentInteraction(holder.experience);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mRoleView;
        public final TextView mCompanyView;
        public final TextView mDateView;
        public Experience experience;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCompanyView = (TextView) view.findViewById(R.id.experience_company);
            mRoleView = (TextView) view.findViewById(R.id.experience_role);
            mDateView = (TextView) view.findViewById(R.id.experience_date);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCompanyView.getText() + "'";
        }
    }
}
