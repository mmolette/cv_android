package com.molette.cv.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.fragments.ExperienceListFragment;
import com.molette.cv.fragments.ExperiencesFragment;
import com.molette.cv.models.Experience;

import java.util.ArrayList;

/**
 * Created by Mathieu on 29/05/2016.
 */

public class ExperiencesSectionsPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Experience> jobs;
    private ArrayList<Experience> internships;

    public ExperiencesSectionsPagerAdapter(FragmentManager fm, ArrayList<Experience> jobs, ArrayList<Experience> internships) {
        super(fm);
        this.jobs = jobs;
        this.internships = internships;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position){

            case ExperiencesFragment.NUM_TAB_JOBS:
                fragment = ExperienceListFragment.newInstance(jobs);
                break;

            case ExperiencesFragment.NUM_TABS_INTERNSHIPS:
                fragment = ExperienceListFragment.newInstance(internships);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {

        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        CharSequence title = "";
        switch (position){

            case ExperiencesFragment.NUM_TAB_JOBS:
                title = CVApp.getContext().getString(R.string.experiences_tab_jobs);
                break;

            case ExperiencesFragment.NUM_TABS_INTERNSHIPS:
                title = CVApp.getContext().getString(R.string.experiences_tab_internships);
                break;
        }
        return title;
    }
}
