package com.molette.cv.adapters;

import android.database.DataSetObserver;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.fragments.SkillsFragment;
import com.molette.cv.models.Skill;
import com.molette.cv.models.SkillCategory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Mathieu on 22/05/2016.
 */

public class SkillsAdapter implements ExpandableListAdapter {

    private List<SkillCategory> skillCategories;

    public SkillsAdapter(List<SkillCategory> skillCategories) {

        this.skillCategories = skillCategories;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return skillCategories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return skillCategories.get(groupPosition).getSkills().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_skill, null);
        }

        SkillCategory skillCategory = skillCategories.get(groupPosition);

        ((ImageView) convertView.findViewById(R.id.cat_skill_icon)).setImageResource(skillCategory.getIcon());
        ((ImageView) convertView.findViewById(R.id.skill_cat_expand_indicator)).setImageResource((isExpanded) ? R.drawable.ic_expand_less_black_36dp : R.drawable.ic_expand_more_black_36dp);
        ((TextView) convertView.findViewById(R.id.category_skill)).setText(skillCategory.getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_child_skill, null);
        }

        Skill skill = skillCategories.get(groupPosition).getSkills().get(childPosition);

        //((ImageView) convertView.findViewById(R.id.skill_icon)).setImageResource(skill.getIcon());
        ((TextView) convertView.findViewById(R.id.category_skill)).setText(skill.getName());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

}
