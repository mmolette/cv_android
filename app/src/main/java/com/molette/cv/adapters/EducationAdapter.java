package com.molette.cv.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.models.School;

import java.util.List;

/**
 * Created by Mathieu on 27/05/2016.
 */

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.ViewHolder> {

    private List<School> schools;

    public EducationAdapter(List<School> schools) {
        this.schools = schools;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_education, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.school = schools.get(position);
        holder.name.setText(holder.school.getName());
        holder.date.setText(String.valueOf(holder.school.getStartDate()) + " - " + String.valueOf(holder.school.getEndDate()));
        holder.place.setText(holder.school.getPlace());
        holder.topic.setText(holder.school.getTopic());
        holder.picture.setImageResource(holder.school.getPicture());
    }

    @Override
    public int getItemCount() {

        return schools.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView name;
        public final TextView date;
        public final TextView place;
        public final TextView topic;
        public final ImageView picture;
        public School school;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name = (TextView) view.findViewById(R.id.school_name);
            date = (TextView) view.findViewById(R.id.school_date);
            place = (TextView) view.findViewById(R.id.school_place);
            topic = (TextView) view.findViewById(R.id.school_topic);
            picture = (ImageView) view.findViewById(R.id.school_picture);
        }

        @Override
        public String toString() {

            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
