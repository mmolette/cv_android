package com.molette.cv.adapters;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.models.Portfolio;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.minHeight;
import static android.R.attr.publicKey;

/**
 * Created by Mathieu on 01/06/2016.
 */

public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.ViewHolder> implements View.OnClickListener{

    private List<Portfolio> portfolios;
    private List<CardView> cardViews = new ArrayList<>();

    public PortfolioAdapter(List<Portfolio> portfolios) {
        this.portfolios = portfolios;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_portfolio, parent, false);

        cardViews.add(cardView);

        return new PortfolioAdapter.ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.project = portfolios.get(position);
        for (String techno : holder.project.getTechnologies()){
            TextView chip = (TextView) LayoutInflater.from(CVApp.getContext()).inflate(R.layout.chip_layout, holder.chipsView, false);
            chip.setText(techno);
            holder.chipsView.addView(chip);
        }
        holder.name.setText(holder.project.getProject());
        holder.description.setText(holder.project.getDescription());
        holder.date.setText(holder.project.getDate());
        holder.picture.setImageResource(holder.project.getLogo());
        holder.actionButton.setText(holder.project.getType().toString());
        holder.actionButton.setTag(position);
        holder.actionButton.setOnClickListener(this);
        holder.expandButton.setTag(position);
        holder.expandButton.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return portfolios.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.expand_button:
                CardView cardView = cardViews.get((int) v.getTag());
                TextView desciption = (TextView) cardView.findViewById(R.id.portfolio_description);
                if(desciption.getVisibility() == View.GONE){
                    ((ImageButton) v).setImageResource(R.drawable.ic_expand_less_black_36dp);
                    desciption.setVisibility(View.VISIBLE);
                }else{
                    ((ImageButton) v).setImageResource(R.drawable.ic_expand_more_black_36dp);
                    desciption.setVisibility(View.GONE);
                }
                break;

            case R.id.action_button:

                Portfolio portfolio = portfolios.get((int) v.getTag());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(portfolio.getUrl()));
                intent = Intent.createChooser(intent, CVApp.getContext().getString(R.string.chooser_browser));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                CVApp.getContext().startActivity(intent);
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView name;
        public final TextView date;
        public final TextView description;
        public final ImageView picture;
        public final LinearLayout chipsView;
        public final Button actionButton;
        public final ImageButton expandButton;
        public Portfolio project;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name = (TextView) view.findViewById(R.id.portfolio_title);
            date = (TextView) view.findViewById(R.id.portfolio_date);
            description = (TextView) view.findViewById(R.id.portfolio_description);
            picture = (ImageView) view.findViewById(R.id.portfolio_logo);
            chipsView = (LinearLayout) view.findViewById(R.id.chipsView);
            actionButton = (Button) view.findViewById(R.id.action_button);
            expandButton = (ImageButton) view.findViewById(R.id.expand_button);
        }

        @Override
        public String toString() {

            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
