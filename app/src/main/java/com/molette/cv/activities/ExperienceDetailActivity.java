package com.molette.cv.activities;

import android.media.Image;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.molette.cv.R;
import com.molette.cv.models.Experience;

public class ExperienceDetailActivity extends AppCompatActivity {

    private Experience experience;

    private ImageView logo;
    private TextView role;
    private TextView company;
    private TextView project;
    private TextView date;
    private TextView place;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experience_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        logo = (ImageView) findViewById(R.id.experience_logo);
        role = (TextView) findViewById(R.id.experience_role);
        company = (TextView) findViewById(R.id.experience_company);
        project = (TextView) findViewById(R.id.experience_project);
        date = (TextView) findViewById(R.id.experience_date);
        place = (TextView) findViewById(R.id.experience_place);
        description = (TextView) findViewById(R.id.experience_description);

        if(getIntent().getExtras() != null){
            experience = (Experience) getIntent().getExtras().get(MainActivity.EXPERIENCE_EXTRA);
            if(experience != null){
                role.setText(experience.getRole());
                company.setText(experience.getCompany());
                project.setText(experience.getProject());
                String startDate = experience.getStartMonth() + " " + experience.getStartYear();
                String endDate = experience.getEndMonth() + " " + experience.getEndYear();
                date.setText(startDate + " - " + endDate);
                place.setText(experience.getPlace());
                description.setText(experience.getDescription());
                logo.setImageResource(experience.getLogo());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
