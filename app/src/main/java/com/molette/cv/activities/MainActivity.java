package com.molette.cv.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.molette.cv.R;
import com.molette.cv.fragments.EducationFragment;
import com.molette.cv.fragments.ExperienceListFragment;
import com.molette.cv.fragments.ExperiencesFragment;
import com.molette.cv.fragments.HobbiesFragment;
import com.molette.cv.fragments.IntroductionFragment;
import com.molette.cv.fragments.PortfolioFragment;
import com.molette.cv.fragments.SkillsFragment;
import com.molette.cv.helpers.ContactHelper;
import com.molette.cv.helpers.InternationalizationHelper;
import com.molette.cv.models.Experience;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener,
        ExperienceListFragment.OnExperienceListFragmentInteractionListener, BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String EXPERIENCE_EXTRA = "experience_extra";
    public static final String TRANSLATED = "TRANSLATED";
    FloatingActionButton fab;

    Toolbar toolbar;
    NavigationView navigationView;
    BottomNavigationView bottomNavigationView;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        fragmentManager = getSupportFragmentManager();

        navigationView.getMenu().getItem(0).setChecked(true);
        fragmentManager.beginTransaction().add(R.id.content_main, new EducationFragment()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        fab.show();
        switch (item.getItemId()){

            case R.id.nav_education:

                fragmentManager.beginTransaction().replace(R.id.content_main, new EducationFragment()).commit();
                break;

            case R.id.nav_skills:

                fragmentManager.beginTransaction().replace(R.id.content_main, new SkillsFragment()).commit();
                break;

            case R.id.nav_experiences:

                fragmentManager.beginTransaction().replace(R.id.content_main, new ExperiencesFragment()).commit();
                break;

            case R.id.nav_portfolio:

                fab.hide();
                fragmentManager.beginTransaction().replace(R.id.content_main, new PortfolioFragment()).commit();
                break;

            case R.id.nav_hobbies:

                fragmentManager.beginTransaction().replace(R.id.content_main, new HobbiesFragment()).commit();
                break;

            case R.id.nav_share:

                ContactHelper.share();
                break;

            case R.id.nav_send:

                ContactHelper.sendEmail();
                break;
        }

        if(item.getItemId() != R.id.nav_send && item.getItemId() != R.id.nav_share){
            toolbar.setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.fab:
                ContactHelper.sendEmail();
                break;
        }
    }

    @Override
    public void onExperienceListFragmentInteraction(Experience experience) {
        if(experience != null){
            Intent intent = new Intent(this, ExperienceDetailActivity.class);
            intent.putExtra(EXPERIENCE_EXTRA, experience);
            startActivity(intent);
        }
    }
}
