package com.molette.cv.fragments;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.molette.cv.R;
import com.molette.cv.adapters.EducationAdapter;
import com.molette.cv.models.School;
import com.molette.cv.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class EducationFragment extends Fragment {

    List<School> schools;

    public EducationFragment() {
        // Required empty public constructor
    }

    public static EducationFragment newInstance() {
        EducationFragment fragment = new EducationFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        schools = new ArrayList<>();

        School supinfo = new School("ESI SUPINFO International University", 2012, 2015, "Lyon, France", getString(R.string.school_topic_supinfo), R.drawable.supinfo);
        School chartreux = new School("Institution des Chartreux", 2010, 2012, "Lyon, France", getString(R.string.school_topic_chartreux), R.drawable.chartreux);
        School stanne = new School("Lycée Sainte-Anne", 2008, 2010, "Roanne, France", getString(R.string.school_topic_arago), R.drawable.arago);

        schools.add(supinfo);
        schools.add(chartreux);
        schools.add(stanne);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_education, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.education_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new EducationAdapter(schools));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
