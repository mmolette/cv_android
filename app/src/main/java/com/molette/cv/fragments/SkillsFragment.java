package com.molette.cv.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.molette.cv.R;
import com.molette.cv.adapters.SkillsAdapter;
import com.molette.cv.models.Skill;
import com.molette.cv.models.SkillCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class SkillsFragment extends Fragment {

    public static final String MOBILE_SKILLS = "Mobile";
    public static final String WEB_SKILLS = "Web";
    public static final String SOFTWARE_SKILLS = "Software";
    public static final String DATABASE_SKILLS = "Database";

    private List<SkillCategory> skillCategories;

    public SkillsFragment() {
        // Required empty public constructor
    }

    public static SkillsFragment newInstance() {
        SkillsFragment fragment = new SkillsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<Skill> mobileSkills = new ArrayList<>();
        mobileSkills.add(new Skill(R.drawable.ic_build_black_24dp, "Android SDK - Java"));
        mobileSkills.add(new Skill(R.drawable.ic_build_black_24dp, "iOS SDK _ Objective-C"));
        mobileSkills.add(new Skill(R.drawable.ic_build_black_24dp, "Windows Phone - C#"));

        List<Skill> webSkills = new ArrayList<>();
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "HTML5"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "CSS3"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "JS (jQuery)"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "PHP5 (Symfony 2)"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "Node.js"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "J2EE (Tomcat, Glassfish)"));
        webSkills.add(new Skill(R.drawable.ic_build_black_24dp, "C# WCF"));

        List<Skill> softwareSkills = new ArrayList<>();
        softwareSkills.add(new Skill(R.drawable.ic_build_black_24dp, "C# WPF (Entity Framework)"));
        softwareSkills.add(new Skill(R.drawable.ic_build_black_24dp, "C++ (Qt)"));
        softwareSkills.add(new Skill(R.drawable.ic_build_black_24dp, "Java"));

        List<Skill> databaseSkills = new ArrayList<>();
        databaseSkills.add(new Skill(R.drawable.ic_build_black_24dp, "MySQL"));
        databaseSkills.add(new Skill(R.drawable.ic_build_black_24dp, "Oracle"));
        databaseSkills.add(new Skill(R.drawable.ic_build_black_24dp, "SQL Server"));

        skillCategories = new ArrayList<>();
        SkillCategory mobileCat = new SkillCategory(R.drawable.ic_smartphone_black_36dp, MOBILE_SKILLS, mobileSkills);
        SkillCategory webCat = new SkillCategory(R.drawable.ic_web_black_36dp, WEB_SKILLS, webSkills);
        SkillCategory softwareCat = new SkillCategory(R.drawable.ic_desktop_windows_black_36dp, SOFTWARE_SKILLS, softwareSkills);
        SkillCategory databaseCat = new SkillCategory(R.drawable.ic_storage_black_36dp, DATABASE_SKILLS, databaseSkills);

        skillCategories.add(mobileCat);
        skillCategories.add(webCat);
        skillCategories.add(softwareCat);
        skillCategories.add(databaseCat);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_skills, container, false);

        ExpandableListView expandableListView = (ExpandableListView) rootView.findViewById(R.id.skillsListView);
        expandableListView.setAdapter(new SkillsAdapter(skillCategories));

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    
}
