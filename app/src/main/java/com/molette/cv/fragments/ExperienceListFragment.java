package com.molette.cv.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.activities.ExperienceDetailActivity;
import com.molette.cv.activities.MainActivity;
import com.molette.cv.adapters.ExperiencesAdapter;
import com.molette.cv.models.Experience;
import com.molette.cv.utils.DividerItemDecoration;

import java.util.ArrayList;

public class ExperienceListFragment extends Fragment {

    private static final String ARG_EXPERIENCES = "experiences";

    private OnExperienceListFragmentInteractionListener mListener;
    private ArrayList<Experience> experiences;

    public ExperienceListFragment() {
    }

    public static ExperienceListFragment newInstance(ArrayList<Experience> experiences) {
        ExperienceListFragment fragment = new ExperienceListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_EXPERIENCES, experiences);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            experiences = getArguments().getParcelableArrayList(ARG_EXPERIENCES);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_experience_list, container, false);

        recyclerView.setLayoutManager(new LinearLayoutManager(CVApp.getContext()));
        recyclerView.setAdapter(new ExperiencesAdapter(experiences, mListener));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));

        return recyclerView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnExperienceListFragmentInteractionListener)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnExperienceListFragmentInteractionListener {

        void onExperienceListFragmentInteraction(Experience experience);
    }
}
