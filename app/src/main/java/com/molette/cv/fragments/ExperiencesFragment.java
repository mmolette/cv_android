package com.molette.cv.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.molette.cv.CVApp;
import com.molette.cv.R;
import com.molette.cv.adapters.ExperiencesSectionsPagerAdapter;
import com.molette.cv.models.Experience;

import java.util.ArrayList;

public class ExperiencesFragment extends Fragment {

    public static final int NUM_TAB_JOBS = 0;
    public static final int NUM_TABS_INTERNSHIPS = 1;

    private ArrayList<Experience> jobs;
    private ArrayList<Experience> interships;

    private ExperiencesSectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager.OnPageChangeListener mPageListener;

    private ViewPager mViewPager;

    public ExperiencesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jobs = new ArrayList<>();
        interships = new ArrayList<>();

        // Jobs
        Experience occ = new Experience("Worldline", "Lille", getString(R.string.job_occ_start_month), getString(R.string.job_occ_end_month),
                "2016", "", getString(R.string.job_occ_role),
                getString(R.string.job_occ_project), getString(R.string.job_occ_description)
                , R.drawable.worldline);

        // Internships
        Experience worldline = new Experience("Worldline", "Lille, France", getString(R.string.job_mcdo_start_month), getString(R.string.job_mcdo_end_month),
                "2015", "2015", getString(R.string.job_mcdo_role),
                getString(R.string.job_mcdo_project),getString(R.string.job_mcdo_description)
                , R.drawable.worldline);

        Experience bsoft = new Experience("Bsoft.fr", "Lyon, France", getString(R.string.job_bsoft_start_month), getString(R.string.job_bsoft_end_month), "2013",
                "2014", getString(R.string.job_bsoft_role),
                getString(R.string.job_bsoft_project), getString(R.string.job_bsoft_description)
                , R.drawable.bsoft);

        Experience cci = new Experience("CCI de Lyon", "Lyon, France", getString(R.string.job_cci_start_month), getString(R.string.job_cci_end_month), "2013",
                "2013", getString(R.string.job_cci_role),
                getString(R.string.job_cci_project), getString(R.string.job_cci_description)
                , R.drawable.cci);

        Experience cinapp = new Experience("Cinapp", "Lyon, France", getString(R.string.job_cinapp_start_month), getString(R.string.job_cinapp_end_month), "2013",
                "2013", getString(R.string.job_cinapp_role),
                getString(R.string.job_cinapp_project), getString(R.string.job_cinapp_description)
                , R.drawable.cinapp);

        Experience aveis = new Experience("Avéis", "Lyon, France", getString(R.string.job_aveis_start_month), getString(R.string.job_aveis_end_month), "2012",
                "2012", getString(R.string.job_aveis_role),
                getString(R.string.job_aveis_project), getString(R.string.job_aveis_description)
                , R.drawable.aveis);

        Experience acrobas = new Experience("Acrobas", "Roanne, France", getString(R.string.job_acrobas_start_month), getString(R.string.job_acrobas_end_month), "2011",
                "2011", getString(R.string.job_acrobas_role),
                getString(R.string.job_acrobas_project), getString(R.string.job_acrobas_description)
                , R.drawable.acrobas);

        jobs.add(occ);
        interships.add(worldline);
        interships.add(bsoft);
        interships.add(cci);
        interships.add(cinapp);
        interships.add(aveis);
        interships.add(acrobas);

        mSectionsPagerAdapter = new ExperiencesSectionsPagerAdapter(getChildFragmentManager(), jobs, interships);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_experiences, container, false);

        mViewPager = (ViewPager) rootView.findViewById(R.id.experiences_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(mPageListener);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.experiences_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
