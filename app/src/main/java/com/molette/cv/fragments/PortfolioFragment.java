package com.molette.cv.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.molette.cv.R;
import com.molette.cv.adapters.PortfolioAdapter;
import com.molette.cv.models.Portfolio;

import java.util.ArrayList;
import java.util.List;

public class PortfolioFragment extends Fragment {

    private List<Portfolio> portfolios;

    public PortfolioFragment() {
        // Required empty public constructor
    }

    public static PortfolioFragment newInstance(String param1, String param2) {
        PortfolioFragment fragment = new PortfolioFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        portfolios = new ArrayList<>();
        List<String> asbvWebTechnos = new ArrayList<>();
        asbvWebTechnos.add("Node.js");
        asbvWebTechnos.add("MongoDb");
        asbvWebTechnos.add("jQuery");
        asbvWebTechnos.add("Bootstrap");
        asbvWebTechnos.add("HTML5");
        asbvWebTechnos.add("CSS3");

        List<String> asbvAndroidTechnos = new ArrayList<>();
        asbvAndroidTechnos.add("Android SDK");
        asbvAndroidTechnos.add("Java");
        asbvAndroidTechnos.add("Volley");
        asbvAndroidTechnos.add("Android Studio");
        asbvAndroidTechnos.add("Git");

        Portfolio asbvAndroid = new Portfolio(R.drawable.asbv, Portfolio.Portfolio_Type.ANDROID,
                getString(R.string.project_asbv_android_title), "2016",
                asbvAndroidTechnos, getString(R.string.project_asbv_android_description),
                "https://play.google.com/store/apps/details?id=basket.villemontais.com.asbv");
        Portfolio asbvWeb = new Portfolio(R.drawable.asbv, Portfolio.Portfolio_Type.WEB,
                getString(R.string.project_asbv_web_title), "2015",
                asbvWebTechnos, getString(R.string.project_asbv_web_description),
                "http://www.villemontaisbasket.fr/");

        portfolios.add(asbvAndroid);
        portfolios.add(asbvWeb);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.portfolio_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new PortfolioAdapter(portfolios));

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
