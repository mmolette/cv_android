package com.molette.cv.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.molette.cv.CVApp;
import com.molette.cv.R;

public class IntroductionFragment extends Fragment implements View.OnTouchListener, View.OnClickListener {

    private static final String WEBSITE_URL = "http://www.molette-mathieu.fr/";

    public IntroductionFragment() {
        // Required empty public constructor
    }

    public static IntroductionFragment newInstance(String param1, String param2) {
        IntroductionFragment fragment = new IntroductionFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_introduction, container, false);
        ImageButton websiteBtn = (ImageButton) rootView.findViewById(R.id.website_button);

        websiteBtn.setOnClickListener(this);
        websiteBtn.setOnTouchListener(this);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.website_button:

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(WEBSITE_URL));
                intent = Intent.createChooser(intent, CVApp.getContext().getString(R.string.chooser_browser));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        Resources resources = CVApp.getContext().getResources();
        Drawable iconPressed = resources.getDrawable(R.drawable.ic_public_white_36dp);
        Drawable icon = resources.getDrawable(R.drawable.ic_public_black_36dp);
        int color = resources.getColor(R.color.white);
        int colorPressed = resources.getColor(R.color.colorApp);

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                v.setBackgroundColor(colorPressed);
                ((ImageButton) v).setImageDrawable(iconPressed);
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_HOVER_EXIT:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_BUTTON_RELEASE:
                v.setBackgroundColor(color);
                ((ImageButton) v).setImageDrawable(icon);
        }

        // false to get onClick
        return false;
    }
}
